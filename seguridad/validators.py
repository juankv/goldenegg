# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import string
from datetime import datetime 

def choice_valido(value):
	if string.find(value , '-') >= 0:
		raise ValidationError('Este campo es obligatorio.')

def cumpleanos_valido(value):
	if value >= datetime.now().date():
		raise ValidationError('Este campo debe tener una fecha menor que la fecha actual.')

def nro_valido(value):
	if not value.isdigit():
		raise ValidationError('Este campo sólo permite números.')