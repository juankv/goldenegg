# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from datetime import datetime

from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_lazy as _

from comun.models import Pais, Departamento, Ciudad

# Create your models here.

class Cliente(models.Model):
	'''
		Esta clase tendrá los datos del perfil de cada usuario en el sistema.
	'''
	NOGESTIONADO = '---------'

	#Tipos de genero
	HOMBRE = 'H'
	MUJER = 'M'
	tipos_genero_choices = (
			('-', '---------'),
			(HOMBRE, _('Hombre')),
			(MUJER, _('Mujer')),			
		)

	#Tipos de identificación 
	CEDULA_CIUDADANIA = 'C.C.'		
	REGISTRO_CIVIL = 'R.C.'
	TARJETA_IDENTIDAD = 'T.I.'
	CEDULA_EXTRANJERIA = 'C.E.'
	PASAPORTE = 'PAS'
	NIT = 'NIT'
	tipos_identificacion_choices = (
			('-', '---------'),
			(CEDULA_CIUDADANIA, _('Cédula de ciudadanía')),
			(REGISTRO_CIVIL, _('Registro civil')),
			(NIT, _('NIT')),
			(CEDULA_EXTRANJERIA, 'Cédula de extranjería'),
			(TARJETA_IDENTIDAD, _('Tarjeta de identidad')),
			(PASAPORTE, _('Pasaporte')),
		)

	#Seguridad	
	usuario = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,)

	#identificación 
	tipo_identificacion = models.CharField( 'Tipo de identificación', max_length = 5, choices = tipos_identificacion_choices, default = '-' ) 
	identificacion= models.CharField('Número de identificación' ,max_length= 20, unique = True )
	

	#Datos Adicionales obligatorios
	cumpleanos = models.DateField('Cumpleaños')
	genero = models.CharField(max_length=1, choices = tipos_genero_choices, default = '-' )
	nro_telefonico = models.CharField('Número telefónico' , max_length = 15 )
	nro_celular = models.CharField( 'Número de celular', max_length= 15 )
	pais = models.ForeignKey(Pais)
	departamento = models.ForeignKey(Departamento)
	ciudad = models.ForeignKey(Ciudad)
	

	def __str__(self):
		return '%s - %s %s' % (self.identificacion, self.usuario.first_name, self.usuario.last_name)

	class Meta:
		default_permissions = ('add', 'change', 'delete', 'view', 'list')
		verbose_name = _('Cliente')
		verbose_name_plural = _('Clientes')
		unique_together = ('tipo_identificacion', 'identificacion',)




	



