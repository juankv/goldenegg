# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.filters import FieldListFilter

from .models import Cliente
from .forms import ClienteForm
from .filters import DecadeBornListFilter, TipoIdListFilter, GeneroListFilter

class ClienteAdmin(admin.ModelAdmin):
	list_display = ("identificacion","tipo_identificacion", "usuario")
	list_filter = (GeneroListFilter,TipoIdListFilter, DecadeBornListFilter, )
	form = ClienteForm

admin.site.register(Cliente, ClienteAdmin)
