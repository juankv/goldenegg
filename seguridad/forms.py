# -*- coding: utf-8 -*-
from datetime import datetime
from django import forms
from django.forms import ModelChoiceField

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from .models import Cliente
from .validators import choice_valido, cumpleanos_valido, nro_valido

from comun.models import Pais, Ciudad, Departamento
from comun.widgets import CalendarMaterializeWidget
from comun.fields import CustomDateField, UsuarioModelChoiceField, ComunModelChoiceField




class ClienteForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(forms.ModelForm, self).__init__(*args, **kwargs)			
		self.own_fields(self)
		self.validation_per_field(self)

	def own_fields(self, *args, **kwargs):
		self.fields['usuario'] = UsuarioModelChoiceField(queryset=User.objects.all())
		self.fields['pais'] = ComunModelChoiceField(queryset=Pais.objects.all(), label = 'País')
		self.fields['departamento'] = ComunModelChoiceField(queryset=Departamento.objects.all())
		self.fields['ciudad'] = ComunModelChoiceField(queryset=Ciudad.objects.all())
		self.fields['cumpleanos'] = CustomDateField(widget = CalendarMaterializeWidget, label= 'Cumpleaños')

	def validation_per_field(self,  *args, **kwargs):
		self.fields['genero'].validators = [choice_valido]
		self.fields['tipo_identificacion'].validators = [choice_valido]
		self.fields['cumpleanos'].validators = [cumpleanos_valido]
		self.fields['identificacion'].validators = [nro_valido]
		self.fields['nro_telefonico'].validators = [nro_valido]
		self.fields['nro_celular'].validators = [nro_valido]

	def clean(self):
		fechaActual = datetime.now()
		cumpleanos = self.cleaned_data.get('cumpleanos')
		cumpleanos = datetime.combine(cumpleanos, datetime.min.time())
		tipoIdentificacion = self.cleaned_data.get('tipo_identificacion')
		diasDesdeElCumpleanos = fechaActual - cumpleanos
		if(diasDesdeElCumpleanos.days < 2555 and tipoIdentificacion == 'T.I.'):
			raise ValidationError("La tarjeta de identidad sólo puede registrarse para mayores de 7 años.")
		if(diasDesdeElCumpleanos.days > 6480 and tipoIdentificacion == 'T.I.' ):
			raise ValidationError("La tarjeta de identidad sólo puede registrarse para menores de 18 años.")
		if(diasDesdeElCumpleanos.days < 6480 and tipoIdentificacion == 'C.C.'):
			raise ValidationError("La cédula de ciudadanía sólo puede registrarse para mayores de 18 años.")


	class Meta:
		model = Cliente
		fields = '__all__'

		error_messages = {
            'identificacion': {
                'max_length': _("El número de indenficación es muy largo."),
                'unique' : _("El número de indentificación ya está en el sistema, por favor comunicarse con el administrador."),
            },
            'nro_telefonico': {
                'max_length': _("El número telefónico es muy largo."),
            },
            'nro_celular': {
                'max_length': _("El número celular es muy largo."),
            },
        }

