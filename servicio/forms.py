# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.forms import ModelChoiceField

from django.utils.translation import ugettext_lazy as _

from .models import Servicio

class ServicioForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(forms.ModelForm, self).__init__(*args, **kwargs)
		self.own_fields(self)

	def own_fields(self, *args, **kwargs):
		self.fields['descripcion'] = forms.CharField(widget=forms.Textarea)

	class Meta:
		model = Servicio
		exclude = ('costo',)