# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from datetime import datetime


from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Servicio(models.Model):
	'''
		Representa el servicio que se prestara al usuario.
	'''
	NOGESTIONADO = '---------'
	TABLET = 'T.'
	COMPUTADOR_ESCRITORIO = 'C.E.'
	COMPUTADOR_PORTATIL = 'C.P.'
	SMARTPHONE = 'T.C.'
	ROUTER = 'R.'
	IMPRESORA = 'I.'
	SMARTTV = 'S.TV'

	dispositivo_choices = (
			('-', '---------'),
			(TABLET, _('Tablet')),
			(COMPUTADOR_ESCRITORIO, _('Computador de escritorio')),
			(COMPUTADOR_PORTATIL, _('Computador portatil')),
			(SMARTPHONE, _('Teléfono celular')),
			(ROUTER, _('Router')),
			(IMPRESORA, _('Impresora')),
			(SMARTTV, _('Smart TV')),
		)


	nombre = models.CharField('Nombre', max_length = 40)
	descripcion = models.CharField('Descripción', max_length = 400)
	costo = models.DecimalField('Costo', max_digits = '9', decimal_places = '2')
	tiempo = models.TimeField('Tiempo')
	dispositivo = models.CharField(max_length = 5, choices = dispositivo_choices, default = '-')
	remoto = models.BooleanField('Remoto')

	class Meta:
		verbose_name = _('Servicio')
		verbose_name_plural = _('Servicios')
		default_permissions = ('add', 'change', 'delete', 'view', 'list')
		db_table = 'servicio'