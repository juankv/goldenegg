# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Servicio
from .forms import ServicioForm

class ServicioAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'costo', 'tiempo', 'remoto',)
	form = ServicioForm

admin.site.register(Servicio, ServicioAdmin)

