# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from django.contrib.auth.models import User
# Create your models here.
# Módulo#Auditoría
class AuditoriaCreacionModel(models.Model):
	'''
		Esta clase la tendran todas las tablas en las cuales se crearan datos, 
		a traves de formularios.
	'''
	creado = models.DateTimeField(auto_now_add = True)
	usuario_creador = models.CharField(max_length = 100)

	class Meta:
		abstract = True

class AuditoriaModificacionModel(models.Model):
	'''
		Esta clase estará unida a todos los módelos que sean modificados a través de 
		un formulario.
	'''
	modificado = models.DateTimeField(auto_now_add = True)
	nombre_usuario = models.CharField(max_length = 100)

# Módulo#Común#Usuario#Seguridad
class Pais(models.Model):
	'''
		El país tiene una relación fuerte con los datos personales del usuario.
	'''
	nombre = models.CharField(max_length= 100)

class Departamento(models.Model):
	'''
		Departamento tiene una relación fuerte con el país.
	'''
	nombre = models.CharField(max_length = 100)
	pais = models.ForeignKey(Pais)

class Ciudad(models.Model):
	'''
		Ciudad del departamento del país.
	'''
	nombre = models.CharField(max_length = 100)
	departamento = models.ForeignKey(Departamento)
