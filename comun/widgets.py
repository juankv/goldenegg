# -*- coding: utf-8 -*-
from django import forms
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string

class CalendarMaterializeWidget(forms.Widget):
	template_name = 'widgets/calendar.html'

	class Media:
		js = (
			  'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.js',
		  'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js',
		  'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-route.min.js',
		  'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js',
		  'https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js',
		  'https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-114/svg-assets-cache.js',
		  'https://cdn.gitcdn.link/cdn/angular/bower-material/v1.1.1/angular-material.js',
		  	'/static/js/jquery-2.1.1.js'
			)

		css = {
			'all': (
					'https://cdn.gitcdn.link/cdn/angular/bower-material/v1.1.1/angular-material.css', 
				'https://material.angularjs.org/1.1.1/docs.css',
				'/static/css/widgets/calendarmaterialize.css',
				)
		}

	def render(self, name, value, attrs=None):
		context= {
			'value' : value,
			'attrs' : attrs,
		}
		return mark_safe(render_to_string(self.template_name, context))