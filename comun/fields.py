# -*- coding: utf-8 -*-

from django import forms

class CustomDateField(forms.DateField):
	def __init__(self, *args, **kwargs):
		kwargs.setdefault('input_formats', ("%m/%d/%Y",))
		super(CustomDateField, self).__init__(*args, **kwargs)

class UsuarioModelChoiceField(forms.ModelChoiceField):
	def label_from_instance(self, obj):
		return "%s %s" % (obj.first_name, obj.last_name)

class ComunModelChoiceField(forms.ModelChoiceField):
	def label_from_instance(self,obj):
		return "%s" % (obj.nombre)
