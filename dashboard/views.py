# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, permission_required
import json

# Create your views here.
@login_required
def index(request):
	return render(request, "dashboard.html")